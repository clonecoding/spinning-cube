# Spinning Cube
회전하는 3D큐브를 2차원 콘솔창에 출력하는 C++프로그램입니다.
<br><br>


## 클론코딩 입니다.
클론코딩 프로젝트이며 유튜버 Servet Gulnaroglu님의 영상
https://www.youtube.com/watch?v=p09i_hoFdd0&list=PLzjqS77Bk58q3qc-f9994XnHd98fsaFEW&index=1
을 참고하였습니다.

## 원본제작자의 프로젝트
https://github.com/servetgulnaroglu/cube.c.git


## 이 프로젝트를 바탕으로 작성된 포스팅
https://velog.io/@cldhfleks2/Rotation3DCube1
