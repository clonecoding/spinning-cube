#define _CRT_SECURE_NO_WARNINGS 
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <windows.h>
/*
* 행렬의 회전을 이용한 3D 큐브를 2D공간에 출력하는 프로그램
* => 회전행렬이 아닌 quarternions를 이용해도 가능할것이다.(다만 기하, 대수학범위까지 확장됨)
* 
*/
float axisX, axisY, axisZ; //각 축의 위치

float cubeSize;//큐브의 실제 크기 너무 크면 큐브의 한면에 큐브의 뒷면이 출력되는 오류가남(float의 한계)
int backgroundWidth = 160, backgroundHeight = 44; //백그라운드 전체 크기(출력공간)
float zBuffer[160 * 44]; //깊이값
char buffer[160 * 44]; //출력할 문자열을 담는공간
int backgroundASCIICode = '.'; //빈공간에 출력할 문자(정수값)
int distanceFromCam = 100; //큐브를 실제로 바라보는 거리(큐브와의 Z축 거리)
float cubeCenterOffset; //시작위치
float incrementSpeed = 0.8f; //큐브마다 개별적으로 설정불가

float calculateX(int i, int j, int k) {
	return j * sin(axisX) * sin(axisY) * cos(axisZ)
		- k * cos(axisX) * sin(axisY) * cos(axisZ)
		+ j * cos(axisX) * sin(axisZ)
		+ k * sin(axisX) * sin(axisZ)
		+ i * cos(axisY) * cos(axisZ);
}

float calculateY(int i, int j, int k) {
	return j * cos(axisX) * cos(axisZ) + k * sin(axisX) * cos(axisZ)
		- j * sin(axisX) * sin(axisY) * sin(axisZ) + k * cos(axisX) * sin(axisY) * sin(axisZ)
		- i * cos(axisY) * sin(axisZ);
}

float calculateZ(int i, int j, int k) {
	return k * cos(axisX) * cos(axisY)
		- j * sin(axisX) * cos(axisY)
		+ i * sin(axisY);
}

//큐브의 각 면의모양을 계산 : zBuffer, buffer작성
void calculateForSurface(float cubeX, float cubeY, float cubeZ, int ch) {
	float x = calculateX(cubeX, cubeY, cubeZ);
	float y = calculateY(cubeX, cubeY, cubeZ);
	float z = calculateZ(cubeX, cubeY, cubeZ) + distanceFromCam; //큐브와 Z축거리만큼 떨어져서 봄

	float OutOfZone = 1 / z; // 2/z, 3/z할수록 큐브가 커진다..?
	float K1 = 40;

	//2D공간에 실제로 출력하는것은 X, Y좌표만임
	int displayX = (int)(backgroundWidth / 2 + cubeCenterOffset + K1 * OutOfZone * x * 2);
	int displayY = (int)(backgroundHeight / 2 + K1 * OutOfZone * y);

	int idx = displayX + displayY * backgroundWidth;
	if (idx >= 0 && idx < backgroundWidth * backgroundHeight && OutOfZone > zBuffer[idx]) {
		zBuffer[idx] = OutOfZone;
		buffer[idx] = ch;
	}
}

int main() {
	printf("%\x1b[2J"); //콘솔창의 윗쪽화면 지우기
	while (1) {
		memset(buffer, backgroundASCIICode, backgroundWidth * backgroundHeight);
		memset(zBuffer, 0, backgroundWidth * backgroundHeight * 4);
		//첫번째 큐브
		cubeSize = 23;
		cubeCenterOffset = -1.5 * cubeSize; //화면 정중앙으로부터 왼쪽으로 2 * cubeSize위치가 큐브의 중심
		for (float cubeX = -cubeSize; cubeX < cubeSize; cubeX += incrementSpeed) {
			for (float cubeY = -cubeSize; cubeY < cubeSize; cubeY += incrementSpeed) {
				calculateForSurface(cubeX, cubeY, -cubeSize, '1'); 
				calculateForSurface(cubeSize, cubeY, cubeX, '2'); //90도
				calculateForSurface(-cubeSize, cubeY, -cubeX, '3'); //-90도
				calculateForSurface(-cubeX, cubeY, cubeSize, '4'); //180도
				calculateForSurface(cubeX, -cubeSize, -cubeY, '5'); //90도
				calculateForSurface(cubeX, cubeSize, cubeY, '6'); //-90도
			}
		}

		//두번째 큐브
		cubeSize = 23;
		cubeCenterOffset = 1.5 * cubeSize;
		for (float cubeX = -cubeSize; cubeX < cubeSize; cubeX += incrementSpeed) {
			for (float cubeY = -cubeSize; cubeY < cubeSize; cubeY += incrementSpeed) {
				calculateForSurface(cubeX, cubeY, -cubeSize, '6');
				calculateForSurface(cubeSize, cubeY, cubeX, '5'); //90도
				calculateForSurface(-cubeSize, cubeY, -cubeX, '4'); //-90도
				calculateForSurface(-cubeX, cubeY, cubeSize, '3'); //180도
				calculateForSurface(cubeX, -cubeSize, -cubeY, '2'); //90도
				calculateForSurface(cubeX, cubeSize, cubeY, '1'); //-90도
			}
		}
		printf("\x1b[H"); //콘솔창의 아랫쪽화면 지우기
		for (int k = 0; k < backgroundWidth * backgroundHeight; k++) {
			putchar(k % backgroundWidth ? buffer[k] : 10);
		}

		//큐브의 각축의방향으로 회전 속도
		axisX += 0.1;
		axisY += 0.1;
		axisZ += 0.1;
		//Sleep(100);
	}

	return 0;
}